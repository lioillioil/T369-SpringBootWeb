package com.bdqn.t369springbootweb.controller;

import com.bdqn.t369springbootweb.pojo.T369;
import com.bdqn.t369springbootweb.service.HelloService;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;


/**
 * HelloController
 *
 * @author LILIBO
 * @since 2024/8/27
 */
@RestController // 该类的所有方法都是Rest风格（直接响应数据）
@RequestMapping("/hello")
public class HelloController {

    /**
     * 读取自定义配置 ${t369.name}
     */
    @Value("${t369.name}")
    private String name;

    // @Value("#{'${t369.teacher}'.split(',')}") // 读不了，换一种方式
    // private String[] teacher;

    @Resource
    private T369 t369;

    // 注入Service层
    @Resource
    private HelloService helloService;

    @GetMapping("/hi/{name}")
    public String hi(@PathVariable("name") String name) {

        return "[" + this.name + "] " + "<br>" + helloService.sayHi(name) + "<br>" + Arrays.toString(t369.getTeacher().toArray());
    }

}
