package com.bdqn.t369springbootweb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * TestController
 *
 * @author LILIBO
 * @since 2024/8/27
 */
@Controller
public class TestController {

    @ResponseBody
    @RequestMapping("/test")
    public String test(int num1, int num2) {
        return "结果是：" + (num1 + num2);
    }

}
