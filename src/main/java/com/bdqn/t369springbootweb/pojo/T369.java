package com.bdqn.t369springbootweb.pojo;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * T369
 *
 * @author LILIBO
 * @since 2024/8/29
 */
@Component // 让Spring管理Bean
@ConfigurationProperties(prefix = "t369") // 配置类注解，通过前置获取其下配置与属性对应
public class T369 {

    private String name;
    private List<String> teacher;
    private List<String> student;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getTeacher() {
        return teacher;
    }

    public void setTeacher(List<String> teacher) {
        this.teacher = teacher;
    }

    public List<String> getStudent() {
        return student;
    }

    public void setStudent(List<String> student) {
        this.student = student;
    }

}
