package com.bdqn.t369springbootweb.service;

/**
 * HelloService
 *
 * @author LILIBO
 * @since 2024/8/27
 */
public interface HelloService {

    /**
     * 问好
     *
     * @param name
     * @return
     */
    String sayHi(String name);

}
