package com.bdqn.t369springbootweb.service.impl;

import com.bdqn.t369springbootweb.service.HelloService;
import org.springframework.stereotype.Service;

/**
 * HelloServiceImpl
 *
 * @author LILIBO
 * @since 2024/8/27
 */
@Service
public class HelloServiceImpl implements HelloService {

    /**
     * 问好
     *
     * @param name
     * @return
     */
    @Override
    public String sayHi(String name) {
        return "您好，" + name;
    }

}
