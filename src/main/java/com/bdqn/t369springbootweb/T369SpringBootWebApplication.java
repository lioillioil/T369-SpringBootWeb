package com.bdqn.t369springbootweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class T369SpringBootWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(T369SpringBootWebApplication.class, args);
    }

}
